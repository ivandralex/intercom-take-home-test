# Intercom take home test

To test and compile this programme you will need Go verion 1.11.

To run tests please use the following command:
```
./run_tests.sh
```

To compile solution please execute the following command:

```
go build main.go
```

In order to print help please use -h flag:

```
./main -h
```

In order to find customers within 100 km from Intercom's office please use the following command:

```
./main -i ./customers.txt
```
This will output answer for this task:
```
Customers within radius 100.00 km from point (53.3394, -6.2577):
UserID=4 Name=Ian Kehoe
UserID=5 Name=Nora Dempsey
UserID=6 Name=Theresa Enright
UserID=8 Name=Eoin Ahearn
UserID=11 Name=Richard Finnegan
UserID=12 Name=Christina McArdle
UserID=13 Name=Olive Ahearn
UserID=15 Name=Michael Ahearn
UserID=17 Name=Patricia Cahill
UserID=23 Name=Eoin Gallagher
UserID=24 Name=Rose Enright
UserID=26 Name=Stephen McArdle
UserID=29 Name=Oliver Ahearn
UserID=30 Name=Nick Enright
UserID=31 Name=Alan Behan
UserID=39 Name=Lisa Ahearn
```
Also you can play around by specifying different coordinates of the office and different radius!