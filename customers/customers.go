package customers

import (
	"bufio"
	"encoding/json"
	"os"
)

func parseCustomer(jsonStr string) (*Customer, error) {
	var customer Customer

	err := json.Unmarshal([]byte(jsonStr), &customer)

	return &customer, err
}

//ReadCustomers reads file and returns stream of customers
func ReadCustomers(inputPath string) ([]*Customer, error) {
	file, err := os.Open(inputPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var list []*Customer

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		customer, err := parseCustomer(scanner.Text())

		if err != nil {
			return nil, err
		}

		list = append(list, customer)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return list, nil
}
