package customers

import (
	"encoding/json"
	"strconv"
)

//Customer is a model for customer
type Customer struct {
	UserID    uint
	Name      string
	Latitude  float64
	Longitude float64
}

//UnmarshalJSON unmarshals json
func (c *Customer) UnmarshalJSON(data []byte) error {
	var rawStrings map[string]interface{}

	err := json.Unmarshal(data, &rawStrings)
	if err != nil {
		return err
	}

	for k, v := range rawStrings {
		if k == "latitude" {
			latitude, err := strconv.ParseFloat(v.(string), 64)

			if err != nil {
				return err
			}

			c.Latitude = latitude
		} else if k == "longitude" {
			longitude, err := strconv.ParseFloat(v.(string), 64)

			if err != nil {
				return err
			}

			c.Longitude = longitude
		} else if k == "user_id" {
			c.UserID = uint(v.(float64))
		} else if k == "name" {
			c.Name = v.(string)
		}

		if err != nil {
			return err
		}
	}

	return nil
}
