package customers

import "sort"

//By is the type of a "less" function that defines the ordering of its Customer arguments.
type By func(p1, p2 *Customer) bool

type customersSorter struct {
	customers []*Customer
	by        func(p1, p2 *Customer) bool
}

//Len is part of sort.Interface.
func (s *customersSorter) Len() int {
	return len(s.customers)
}

//Less is part of sort.Interface
func (s *customersSorter) Less(i, j int) bool {
	return s.by(s.customers[i], s.customers[j])
}

//Swap is part of sort.Interface.
func (s *customersSorter) Swap(i, j int) {
	s.customers[i], s.customers[j] = s.customers[j], s.customers[i]
}

//Sort is a method on the function type, By, that sorts the argument slice according to the function.
func (by By) Sort(customers []*Customer) {
	rs := &customersSorter{
		customers: customers,
		by:        by,
	}
	sort.Sort(rs)
}

//SortByUserID sorts customers by user id
func SortByUserID(customers *[]*Customer) {
	priceOrder := func(c1, c2 *Customer) bool {
		return c2.UserID > c1.UserID
	}

	By(priceOrder).Sort(*customers)
}
