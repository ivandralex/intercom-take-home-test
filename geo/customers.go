package geo

import "intercom.com/take-home-test/customers"

//FindCustomersInRadius finds customers within specified radius from point specified by latitude, longitude
func FindCustomersInRadius(points []*customers.Customer, latitude float64, longitude float64, radius float64) []*customers.Customer {
	var filtered []*customers.Customer

	var distance float64

	for _, customer := range points {
		distance = GetGreatCircleDistance(latitude, longitude, customer.Latitude, customer.Longitude)

		if distance <= radius {
			filtered = append(filtered, customer)
		}
	}

	return filtered
}
