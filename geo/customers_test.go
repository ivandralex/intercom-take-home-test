package geo

import (
	"math"
	"testing"

	"intercom.com/take-home-test/customers"
)

func TestFindCustomersInRadius(t *testing.T) {
	points := []*customers.Customer{
		&customers.Customer{
			UserID:    1,
			Name:      "South Pole",
			Latitude:  -90,
			Longitude: 0,
		},
		&customers.Customer{
			UserID:    2,
			Name:      "North Pole",
			Latitude:  90,
			Longitude: 0,
		},
		&customers.Customer{
			UserID:    3,
			Name:      "Null Island",
			Latitude:  0,
			Longitude: 0,
		},
		&customers.Customer{
			UserID:    4,
			Name:      "Theresa Enright",
			Latitude:  53.1229599,
			Longitude: -6.2705202,
		},
	}

	filtered := FindCustomersInRadius(points, 0, 0, 100)

	if len(filtered) != 1 || filtered[0].UserID != 3 {
		t.Error("should return Null Island only")
	}

	filtered = FindCustomersInRadius(points, 90, 0, math.Pi*MeanEarthRadius/2)

	if len(filtered) != 3 {
		t.Errorf("should return 3 customers")
	}

	//should return customers located in the North hemisphere
	for _, customer := range filtered {
		if customer.UserID == 1 {
			t.Errorf("should not contain South Pole")
		}
	}

}
