package geo

import (
	"math"
)

//MeanEarthRadius is a mean Earth radius (assuming Earth is ellipsoid)
const MeanEarthRadius = 6371000

func toRadians(angle float64) float64 {
	return angle / 180.0 * math.Pi
}

//GetGreatCircleDistance calculates great circle distance between two points
func GetGreatCircleDistance(lat1 float64, lon1 float64, lat2 float64, lon2 float64) float64 {
	latRad1 := toRadians(lat1)
	lonRad1 := toRadians(lon1)

	latRad2 := toRadians(lat2)
	lonRad2 := toRadians(lon2)

	deltaLon := math.Abs(lonRad1 - lonRad2)

	centralAngle := math.Acos(math.Sin(latRad1)*math.Sin(latRad2) + math.Cos(latRad1)*math.Cos(latRad2)*math.Cos(deltaLon))

	return MeanEarthRadius * centralAngle
}
