package geo

import (
	"math"
	"testing"
)

func TestGetGreatCircleDistance(t *testing.T) {
	//distance between two coinciding points
	distance := GetGreatCircleDistance(50, 15, 50, 15)

	//it is obviously 0
	expected := 0.0

	if distance != expected {
		t.Errorf("distance is incorrect: expected %.2f got %.2f", expected, distance)
	}

	//distance to the opposite side of the Earth
	distance = GetGreatCircleDistance(0, 0, 180, 0)

	//it is a half of a full circle length
	expected = math.Pi * MeanEarthRadius

	if distance != expected {
		t.Errorf("distance is incorrect: expected %.2f got %.2f", expected, distance)
	}

	//distance from equator to the pole
	distance = GetGreatCircleDistance(0, 0, 180, 90)

	//it's 1/4 of a full cirlce length
	expected /= 2

	if distance != expected {
		t.Errorf("distance is incorrect: expected %.2f got %.2f", expected, distance)
	}

	//distance between two random points
	distance = GetGreatCircleDistance(52.3191841, -8.5072391, 53.807778, -7.714444)
	expected = 173791.6390085542

	if math.Abs(distance-expected) < 1e-11 {
		t.Errorf("distance is incorrect: expected %.2f got %.2f", expected, distance)
	}
}
