package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"

	"intercom.com/take-home-test/customers"
	"intercom.com/take-home-test/geo"
)

type config struct {
	filePath  string
	latitude  float64
	longitude float64
	radius    float64
}

func parseFlags() (*config, error) {
	var inputPath = flag.String("i", "", "path to local input file")
	var latStr = flag.String("lat", "53.339428", "office latitude (floating point number)")
	var lonStr = flag.String("lon", "-6.257664", "office longitude (floating point number)")
	var rStr = flag.String("r", "100", "radius in kilometers in which to look for customers")

	flag.Parse()

	if *inputPath == "" {
		return nil, fmt.Errorf("input path is mandatory")
	}

	latitude, err := strconv.ParseFloat(*latStr, 64)

	if err != nil {
		return nil, err
	}

	longitide, err := strconv.ParseFloat(*lonStr, 64)

	if err != nil {
		return nil, err
	}

	radius, err := strconv.ParseFloat(*rStr, 64)

	if err != nil {
		return nil, err
	}

	return &config{
		filePath:  *inputPath,
		latitude:  latitude,
		longitude: longitide,
		radius:    radius,
	}, nil
}

func main() {
	c, err := parseFlags()

	if err != nil {
		log.Fatal(err)
	}

	list, err := customers.ReadCustomers(c.filePath)

	if err != nil {
		log.Fatalf("failed to read input file: %s", err)
	}

	filteredCustomers := geo.FindCustomersInRadius(list, c.latitude, c.longitude, c.radius*1000)

	customers.SortByUserID(&filteredCustomers)

	if len(filteredCustomers) == 0 {
		fmt.Printf("No customers within radius %.2f km from point (%.4f, %.4f).\n", c.radius, c.latitude, c.longitude)
		return
	}

	fmt.Printf("Customers within radius %.2f km from point (%.4f, %.4f):\n", c.radius, c.latitude, c.longitude)

	for _, customer := range filteredCustomers {
		fmt.Printf("UserID=%d Name=%s\n", customer.UserID, customer.Name)
	}
}
